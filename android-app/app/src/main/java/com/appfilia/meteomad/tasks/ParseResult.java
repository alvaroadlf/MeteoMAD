package com.appfilia.meteomad.tasks;

public enum ParseResult {OK, JSON_EXCEPTION, CITY_NOT_FOUND}