package com.appfilia.meteomad;

public class Constants {
    public static final String  DEFAULT_CITY = "Madrid";
    public static final String  DEFAULT_LAT = "40.418889";
    public static final String  DEFAULT_LON = "-3.691944";
}
